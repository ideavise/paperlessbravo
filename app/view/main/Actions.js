Ext.define('Paperless.view.main.Actions', {
    extend: 'Ext.app.Controller',
    
    alias: 'custom',
    
    /* -----------------------------------------------------------------------
    Go Previous Dashboard:
    Finds the last dashboard visited by the user and opens it
    __________________________________________________________________________ */
    
    gopreviousdashboardHandler: function (sp) {
        var arr = this.main.render.getHistory();
        
        for (var i=arr.length,l=0;i>l;i--){
            var item = arr.pop();
            var src = item.source||item.redirectsource;
            if (/dashboard\b/.test(String(src))) {
                this.main.actions.perform(item);
                break;
            }
        }
    },
    
    gobackHandler: function (sp) {
        var arr = this.main.render.getHistory();
        var last = arr.pop();
        var item = arr.pop();
        this.main.actions.perform(item);
    },
    
    triggerrequiredHandler: function (sp) {
        this.main.rules.broadcastTopic({topic:'{activetestsession}'});
        this.main.rules.broadcastTopic({topic:'requiredchange'});
    },
    
    /* -----------------------------------------------------------------------
    Start DCF:
    Opens the DCF form for the selected test session and sets providers for the
    residence and mailing locations.
    __________________________________________________________________________ */
    
    startdcfHandler: function (sp) {
        var ats = this.main.data.getProvider("activetestsession");
           
        var bforms = this.main.data.getObjectByName('dcorbl');
        var disablebl = (ats.Blood===true) ? false : true;
        bforms.disabled = disablebl;
        
        var uforms = this.main.data.getObjectByName('dcorur');
        var disableur = (ats.Urine===true) ? false : true;
        uforms.disabled = disableur;
           
        this.main.actions.perform({
            cmd: 'goto',
            redirectsource: 'dcfForm',
            target: 'userinterface'
        });
    },
    
    /* -----------------------------------------------------------------------
    Toggle Blood forms:
    Enable/Disable blood forms when the Blood checkbox is toggled
    __________________________________________________________________________ */
    
    togglebloodformsHandler: function (sp) {
        var ats = this.main.data.getProvider("activetestsession");
        var btab = this.main.util.get('sessionmanager').tabBar.items.items[4];
        var bforms = this.main.util.get('dcorbl');
        if (ats.Blood===true) {
            bforms.enable(true);
            btab.enable(true);
        } else {
            bforms.disable(true);
            btab.disable(true);
        }
        
        this.main.rules.broadcastTopic({topic:'{activetestsession}'});
//        this.main.rules.broadcastTopic({topic:'refresh',component:sm});
        this.main.rules.broadcastTopic({topic:'requiredchange'});

    },
    
    /* -----------------------------------------------------------------------
    Toggle Blood forms:
    Enable/Disable blood forms when the Blood checkbox is toggled
    __________________________________________________________________________ */
    
    toggleurineformsHandler: function (sp) {
        var ats = this.main.data.getProvider("activetestsession");
        var utab = this.main.util.get('sessionmanager').tabBar.items.items[5];
        var uforms = this.main.util.get('dcorur');
        if (ats.Urine===true) {
            uforms.enable(true);
            utab.enable(true);
        } else {
            uforms.disable(true);
            utab.disable(true);
        }
        
        this.main.rules.broadcastTopic({topic:'{activetestsession}'});
//        this.main.rules.broadcastTopic({topic:'refresh',component:sm});
        this.main.rules.broadcastTopic({topic:'requiredchange'});

    },
    
    refreshdeclarationslistHandler: function (sp) {
        var deccmp = this.main.util.get('declarations');
        if (deccmp) deccmp.refresh();
    },
    
    /* -----------------------------------------------------------------------
    Refresh DCF Review:
    Refresh the review form in the Signatures tab
    __________________________________________________________________________ */
    
    refreshdcfreviewHandler: function (sp) {
        var review = this.main.util.get('dcfReview');
        if (review) review.loadData();
    },
    
    /* -----------------------------------------------------------------------
    Copy Notification Date:
    Copies the notification date from the notification form to the athlete info
    form.
    __________________________________________________________________________ */
    
    copynotificationdateHandler: function (sp) {
        var ats = this.main.data.getProvider("activetestsession");
        
        this.main.actions.perform({
            cmd: "put",
            component: this,
            objects: [ats],
            ArrivalDate: ats.NotificationDate,
            ArrivalTime: ats.NotificationTime
        });
        
        var adate = this.main.util.get('ArrivalDate');
        var atime = this.main.util.get('ArrivalTime');
        
        adate.setValue(ats.NotificationDate);
        adate.commitValue();
        
//        atime.setValue(ats.NotificationTime);
//        atime.commitValue(atime,ats.NotificationTime,null);

        this.main.rules.broadcastTopic({topic:'{activetestsession}'});
        this.main.rules.broadcastTopic({topic:'requiredchange'});

    },
    
    /* -----------------------------------------------------------------------
    Copy Address:
    Copies the values in the Residence Address form to the matching fields
    in the Mailing Address form.
    __________________________________________________________________________ */
    
    copyaddressHandler: function (sp) {
//        debugger;
        
        if (sp.component.value==true) {
            var ats = this.main.data.getProvider("activetestsession");
           
            this.main.actions.perform({
                cmd: "put",
                component: this,
                objects: [ats],
                MailingAddr1: ats.ResidenceAddr1,
                MailingCity: ats.ResidenceCity,
                MailingState: ats.ResidenceState,
                MailingZip: ats.ResidenceZip,
                MailingCountry: ats.ResidenceCountry
            });
           
           // If first field in the mailing address set is triggered
           // the rest of the fields are populated automatically from
           // the values in the active test session
            this.main.util.get('MailingAddr1').setValue(ats.ResidenceAddr1);
            this.main.util.get('MailingAddr1').commitValue();

            this.main.rules.broadcastTopic({topic:'{activetestsession}'});
            this.main.rules.broadcastTopic({topic:'requiredchange'});
           
        }
        
    },
    
    doLoadActiveDocumentHandler: function (sp) {
        var ad = this.main.data.getProvider("activedocument");
        
        var doccmp = this.main.data.getObjectByName('DocumentReviewer');
        doccmp.ct = ad.ct;
        
        
        this.main.actions.perform({
            cmd: 'goto',
            redirectsource: 'docreviewpanel',
            target: 'userinterface'
        });
    },
    
    
    /* -----------------------------------------------------------------------
    startuar:
    Starts an Unsuccessful Attempt Report (UAR) for the test session:  If a UAR
    already exists for the test session it is used again, otherwise a new UAR
    document is created; Starts providers for the two locations in the UAR.
    __________________________________________________________________________ */
    
    startuarHandler: function (sp) {
        
        var par = this.main.data.getProvider("activetestsession");
        
        // Mark the test session as opened
        this.main.actions.perform({
            cmd: "put",
            component: this,
            objects: [par],
            Status: 'OPENED'
        });

        // Find if the test session already contains a UAR
        var desc = this.main.data.getDescendants(par.a);
        var arr = this.main.data.filter(desc,'cid EQ uar');
        
        if (arr.length > 0){
            this.startuarCallback(arr);
        } else {
            var aarr = this.main.data.filter(desc,'cid EQ athlete');
            var apn,spt;
            if (aarr.length>0) {
               apn = aarr[0].AthletePrintedName;
               spt = aarr[0].Sport;
           }
        
            this.main.data.create({
                callback: this.startuarCallback,
                component: sp.component,
                scope: this,
                data: [{
                    cid: 'uar',
                    l:  'Unsuccessful Attempt',
                    pa: par.ca,
                    t: 0,
                    AthletePrintedName: apn,
                    Sport: spt,
                    TestingAuthority: par.TestingAuthority,
                    AttemptType: par.AttemptType
                }]
            });
        }
        
    },
    
    startuarCallback: function (rsp) {
    
        // set the uar to the activeuar provider and open the uar form
        var me = (rsp.scope||this);
        var ts = me.main.data.getProvider("activetestsession");
        var uar, loc1, loc2;
        if (rsp.objects) {
            // A new UAR object was created
            uar = rsp.objects[0];
           
        } else {
            // UAR already exists
            uar = rsp[0];
        }
        
        me.main.data.setProvider("activeuar",uar.a);
        
        // enable/disable first location phone calls
        var phonelist = me.main.data.getObjectByName('uarfirstnumberscalledlist');

        if (uar.FirstCallsPlaced==true) {
           phonelist.disabled=false;
        } else {
           phonelist.disabled=true;
        }
        
        // enable/disable second location fields
        
        var loc = me.main.data.getObjectByName('secondlocfs');
        var time = me.main.data.getObjectByName('secondloctimefs');
        var contact = me.main.data.getObjectByName('secondlocpersoncontactedfs');
        var access = me.main.data.getObjectByName('secondlocrestrictedaccessfs');
        var phone = me.main.data.getObjectByName('secondloccallsplacedfs');
        var secondphonelist = me.main.data.getObjectByName('uarsecondnumberscalledlist');
        var comments = me.main.data.getObjectByName('secondloccommentsfs');
        
        if (uar.secondlocationvisited=="Yes") {
            loc.disabled=false;
            time.disabled=false;
            contact.disabled=false;
            access.disabled=false;
            phone.disabled=false;
            secondphonelist.disabled = (uar.SecondCallsPlaced==true) ? false : true;
            comments.disabled=false;
        } else {
            loc.disabled=true;
            time.disabled=true;
            contact.disabled=true;
            access.disabled=true;
            phone.disabled=true;
            secondphonelist.disabled=true;
            comments.disabled=true;
        }
        
        me.main.actions.perform({
            cmd: 'request',
            source: 'uarpanel',
            target: 'userinterface',
            provider: 'activeuar'
        });
    },
    
    /* -----------------------------------------------------------------------
    uarfirstloccallsplaced:
    Toggles disabled/enabled state of phone call field for first location of uar
    __________________________________________________________________________ */
    
    uarfirstloccallsplacedHandler: function (sp) {
        var auar = this.main.data.getProvider("activeuar");
        var phonelist = this.main.util.get('uarfirstnumberscalledlist');
        var phonedetails = this.main.util.get('uarfirstphonedetailsfs');

        if (auar.FirstCallsPlaced==true) {
           phonelist.enable(true);
           phonedetails.enable(true);
        } else {
           phonelist.disable(true);
           phonedetails.disable(true);
        }
       
    },
    
    /* -----------------------------------------------------------------------
    uarsecondloccallsplaced:
    Toggles disabled/enabled state of phone call field for second location of uar
    __________________________________________________________________________ */
    
    uarsecondloccallsplacedHandler: function (sp) {
        var auar = this.main.data.getProvider("activeuar");
        var phonelist = this.main.util.get('uarsecondnumberscalledlist');
        var phonedetails = this.main.util.get('uarsecondphonedetailsfs');

        if (auar.SecondCallsPlaced==true) {
           phonelist.enable(true);
           phonedetails.enable(true);
        } else {
           phonelist.disable(true);
           phonedetails.disable(true);
        }
       
    },
    
    /* -----------------------------------------------------------------------
    secondlocvisited:
    Toggles disabled/enabled state of second location fields in uar
    __________________________________________________________________________ */
    
    secondlocvisitedHandler: function (sp) {
        var uar = this.main.data.getProvider("activeuar");
        var loc = this.main.util.get('secondlocfs');
        var time = this.main.util.get('secondloctimefs');
        var contact = this.main.util.get('secondlocpersoncontactedfs');
        var access = this.main.util.get('secondlocrestrictedaccessfs');
        var phone = this.main.util.get('secondloccallsplacedfs');
        var phonelist = this.main.util.get('uarsecondnumberscalledlist');
        var phonedetails = this.main.util.get('uarsecondphonedetailsfs');
        var comments = this.main.util.get('secondloccommentsfs');
        
        if (uar.secondlocationvisited=="Yes") {
            loc.enable(true);
            time.enable(true);
            contact.enable(true);
            access.enable(true);
            phone.enable(true);
            comments.enable(true);
            if (uar.SecondCallsPlaced==true) {
                phonelist.enable(true);
                phonedetails.enable(true);
            }
        } else {
            loc.disable(true);
            time.disable(true);
            contact.disable(true);
            access.disable(true);
            phone.disable(true);
            phonelist.disable(true);
            phonedetails.disable(true);
            comments.disable(true);
        }
        
        
        this.main.rules.broadcastTopic({topic:'requiredchange'});
        
        
    },
    
    /* -----------------------------------------------------------------------
    submituar:
    Submits an Unsuccessful Attempt Report (UAR): creates UAR document; check
    that times are valid; display confirmation message; update status of 
    associated test session
    __________________________________________________________________________ */
    
    submituarHandler: function (sp) {
        var auar = this.main.data.getProvider("activeuar");
        
        // check that the times entered by user are valid
        var msgarr = [];
        var timemsg = '<div>You must correct the following time values:</div>';
        var timepass = false;
        if (auar.FirstArrivalTime>auar.FirstAttemptStart) msgarr.push('First Location: Arrival Time is later than Attempt Start');
        if (auar.FirstArrivalTime>auar.FirstAttemptEnd) msgarr.push('First Location: Arrival Time is later than Attempt End');
        if (auar.FirstAttemptStart>auar.FirstAttemptEnd) msgarr.push('First Location: Attempt Start is later than Attempt End');
        if (auar.SecondArrivalTime>auar.SecondAttemptStart) msgarr.push('Second Location: Arrival Time is later than Attempt Start');
        if (auar.SecondArrivalTime>auar.SecondAttemptEnd) msgarr.push('Second Location: Arrival Time is later than Attempt End');
        if (auar.SecondAttemptStart>auar.SecondAttemptEnd) msgarr.push('Second Location: Attempt Start is later than Attempt End');
        
        for (var i=0,l=msgarr.length;i<l;i++) {
           timemsg = timemsg + '<div>' + msgarr[i] + '</div>';
           timepass = true;
        }
        
        if (timepass) {
           this.main.feedback.say({
                title: 'Invalid Time Found',
                message: timemsg
            });
            return;
        }
        
        var cb = Ext.bind(this.submituarCallback,this);
        
        Ext.Msg.show({
            title: 'Submit Confirmation',
            message: 'All information, particularly the Comments Sections has been thoroughly reviewed and phrases such as â€œmissed testâ€ or â€œwhereabouts failureâ€ have been avoided, as well as ensuring all spelling and grammar is accurate?',
            buttons: Ext.Msg.YESNO,
            icon: Ext.Msg.QUESTION,
            fn: cb
        });
    },
    
    submituarCallback: function (rsp) {
        var me = (rsp.scope||this);
        
        if (rsp==='no') {
            return;
        }
        
        var ts = new Date();
        var auar = me.main.data.getProvider("activeuar");
        var ats = me.main.data.getProvider("activetestsession");
        // Get the active uar and it's descendants
        var report = this.main.data.map[auar.a];

        // Get the template from the dcf Review form and apply test session data to it
        var tpl = new Ext.XTemplate(me.main.data.getObjectByName('UAROutput').ct,me.main.util.getTemplateFunctions());
        var output = tpl.apply(report);
        
        
        me.main.data.create({
            callback: me.submituarFeedback,
            component: me,
            scope: me,
            data: [{
                cid: 'document',
                pa: ats.a,
                l: 'Unsuccessful Attempt Report ' + Ext.Date.format(ts,'m-d-Y h:i A'),
                t: 0,
                timestamp: ts,
                doctype: 'uar',
                ct: output,
                a: ats.a + '.uar'
            }]
        });
        
        // Update test session status
        me.main.actions.perform({
            cmd: "put",
            component: me,
            objects: [ats],
            Status: 'COMPLETED',
            UARCompleted: 1,
            DocumentGenerated: ts
        });
    },
    
    submituarFeedback: function (rsp) {
        var me = (rsp.scope||this);
        
        me.gopreviousdashboardHandler(rsp);
        
        Ext.defer(function () {
            this.main.feedback.say({
                title: 'Success',
                message: 'Unsuccessful Attempt Report saved.'
            })
        },
        1200,
        this)
    },

    
    /* -----------------------------------------------------------------------
    startaar:
    Starts an After Action Report (AAR) for the test session:  If a AAR
    already exists for the test session it is used again, otherwise a new AAR
    document is created; Starts providers for the sub-objects in the AAR.
    __________________________________________________________________________ */
    
    startaarHandler: function (sp) {
        
        var par = this.main.data.getProvider("activetestsession");
        
        // Find if the test session already contains an AAR
        var desc = this.main.data.getDescendants(par.a);
        var arr = this.main.data.filter(desc,'cid EQ aar');
        
        // Get the labs that the shipped samples were sent to and date sent
        var ucla, smrtl = false;
        var sdate;
        var uarr = this.main.data.filter(desc,'cid EQ urinesample');
        for (var i=0,l=uarr.length;i<l;i++) {
            if (uarr[i].SampleStatus=='SHIPPED') {
                if (uarr[i].EPO=='1' || uarr[i].CIRIRMS=='1' || uarr[i].GHRP=='1') ucla=true;
                if (uarr[i].EPO=='2' || uarr[i].CIRIRMS=='2' || uarr[i].GHRP=='2') smrtl=true;
                sdate = uarr[i].DateShipped;
            }
        }
        
        var barr = this.main.data.filter(desc,'cid EQ bloodsample');
        for (var i=0,l=barr.length;i<l;i++) {
            if (barr[i].SampleStatus=='SHIPPED') {
                if (barr[i].Parameters=='1' || barr[i].HGH=='1' || barr[i].HGH2=='1' || barr[i].HBOC=='1' || barr[i].CERA=='1' || barr[i].HBT=='1' || barr[i].EPOBlood=='1') ucla=true;
                if (barr[i].Parameters=='2' || barr[i].HGH=='2' || barr[i].HGH2=='2' || barr[i].HBOC=='2' || barr[i].CERA=='2' || barr[i].HBT=='2' || barr[i].EPOBlood=='2') smrtl=true;
                sdate = barr[i].DateShipped;
            }
        }
        
        if (arr.length > 0){
//            var aardesc = this.main.data.getDescendants(arr[0].ca);
//            var locs = this.main.data.filter(aardesc,'cid EQ location');
//            var comb = arr.concat(locs);
            this.main.data.setProvider("activeaarlocation",{});
            this.startaarCallback(arr);
        } else {
            var atharr = this.main.data.filter(desc,'cid EQ athlete');
            var apn = atharr[0].AthletePrintedName;

            this.main.data.create({
                callback: this.startaarCallback,
                component: sp.component,
                scope: this,
                data: [{
                    cid: 'aar',
                    l:  'After Action Report - ' + apn,
                    pa: par.a,
                    t: 0,
                    LOCTArrivalTime: par.ArrivalTime,
                    LOCTNotificationTime: par.NotificationTime,
                    AfterActionUCLA: ucla,
                    AfterActionSMRTL: smrtl,
                    AfterActionDateSent: sdate
                }]
            });
        }
        
    },
    
    startaarCallback: function (rsp) {
    
        // set the aar to the activeaar provider and open the aar form
        var me = (rsp.scope||this);
        var ts = me.main.data.getProvider("activetestsession");
        var aar, loc1;
        if (rsp.objects) {
            // A new AAR object was created
            aar = rsp.objects[0];
        } else {
            // AAR already exists
            aar = rsp[0];
        }
    
        me.main.data.setProvider("activeaar",aar.a);
        
        me.main.actions.perform({
            cmd: 'request',
            source: 'aarpanel',
            target: 'userinterface',
            provider: 'activeaar'
        });
    },
    
    completedtestlocationCallback: function (rsp) {
        var me = (rsp.scope||this);
        me.main.data.setProvider("completedtestlocation",rsp.objects[0].a);
        // Create object for the completed test location phone call
        me.main.data.create({
            callback: me.completedtestphoneCallback,
            component: rsp.cmp,
            scope: me,
            data: [{
                cid: 'phonecall',
                l: 'Completed Test Phone Call',
                pa: rsp.objects[0].a,
                t: 0
            }]
        });
    },

    completedtestphoneCallback: function (rsp) {
        var me = (rsp.scope||this);
        me.main.data.setProvider("completedtestphonecall",rsp.objects[0].a);
    },
    
     /* -----------------------------------------------------------------------
    athleteuncooperative:
    Sets response field to required if athlete is uncooperative
    __________________________________________________________________________ */
       
    athleteuncooperativeHandler: function (sp) {
        var aar = this.main.data.getProvider("activeaar");
        var comments = this.main.data.getObjectByName('AAProcessingComments');
        
        if (aar.AfterActionCooperative=="Yes") {
           comments.required=false;
        } else {
            comments.required=true;
        }

        this.main.rules.broadcastTopic({topic:'{activeaar}'});
        this.main.rules.broadcastTopic({topic:'refresh',component:comments});
        this.main.rules.broadcastTopic({topic:'requiredchange'});
        
    },
    
    /* -----------------------------------------------------------------------
    submitaar:
    Submits an After Action Report (AAR): creates AAR document
    __________________________________________________________________________ */
    
    submitaarHandler: function (sp) {
        var aar = this.main.data.getProvider("activeaar");
        var ats = this.main.data.getProvider("activetestsession");
        // timestamp
        var ts = new Date();

        // Get the active arr and it's descendants
        var report = this.main.data.map[aar.a];

        // Get the template from the dcf Review form and apply test session data to it
        var tpl = new Ext.XTemplate(this.main.data.getObjectByName('AAROutput').ct,this.main.util.getTemplateFunctions());
        var output = tpl.apply(report);
        
        // Create the After Action Report inside of the active test session
        this.main.data.create({
            callback: this.submitaarFeedback,
            component: this,
            scope: this,
            data: [{
                cid: 'document',
                pa: ats.a,
                l: 'After Action Report ' + Ext.Date.format(ts,'m-d-Y h:i A'),
                t: 0,
                timestamp: ts,
                doctype: 'aar',
                ct: output,
                a: ats.a + '.aar'
            }]
        });
        
        // Update test session status
        this.main.actions.perform({
            cmd: "put",
            component: this,
            objects: [ats],
            AfterActionCompleted: 1
        });
    },
    
    submitaarFeedback: function (rsp) {
        var me = (rsp.scope||this);
        
        me.gopreviousdashboardHandler(rsp);
        
        Ext.defer(function () {
            this.main.feedback.say({
                title: 'Success',
                message: 'After Action Report saved.'
            })
        },
        1200,
        this)
    },
    
    /* -----------------------------------------------------------------------
    submitsession:
    Saves the test session: Updates the test session status to COMPLETED; creates
    dcor document; Sends email to athlete; builds samples
    __________________________________________________________________________ */
    
    submitsessionHandler: function (sp) {
        var ats = this.main.data.getProvider("activetestsession");
        var ts = new Date();
        
//        debugger;
        
        // Create DCOR document
        var dcorlbl = (ats.designation=='nonevent') ? 'DCOR ' + Ext.Date.format(ts,'m-d-Y h:i A') : 'DCOR ' + ats.AthletePrintedName + ' - ' + Ext.Date.format(ts,'m-d-Y h:i A');
        // Find if the test session already contains a DCOR
        var desc = this.main.data.getDescendants(ats.a);
        var arr = this.main.data.filter(desc,'cid EQ document AND doctype EQ dcor');
        
        // Get the active test session and it's descendants
        var report = this.main.data.map[ats.a];
        
        // Get the template from the dcf Review form and apply test session data to it
        var tpl = new Ext.XTemplate(this.main.data.getObjectByName('dcfReview').ct,this.main.util.getTemplateFunctions());
        var output = tpl.apply(report);


        // Update test session status
        this.main.actions.perform({
            cmd: "put",
            component: this,
            objects: [ats],
            Status: 'COMPLETED',
            DocumentGenerated: ts,
            CheckedOut: 'Completed'
        });
        
        // Update status of urine samples
        var desc = this.main.data.getDescendants(ats.ca);
        var uarr = this.main.data.filter(desc, 'cid EQ urinesample');
        
        this.main.actions.perform({
            cmd: 'put',
            component: this,
            objects: uarr,
            SampleStatus: 'AVAILABLE'
        });

        // Update status of blood samples
        var barr = this.main.data.filter(desc, 'cid EQ bloodsample');
        
        this.main.actions.perform({
            cmd: 'put',
            component: this,
            objects: barr,
            SampleStatus: 'AVAILABLE'
        });
        
        if ((Ext.isArray(arr)) && (arr.length>0)) {
            // document already exists
        } else {
            this.main.data.create({
                callback: this.submitsessionCallback,
                component: this,
                scope: this,
                data: [{
                    cid: 'document',
                    pa: ats.a,
                    l: dcorlbl,
                    t: 0,
                    notification: ats.PrimaryEmail,
                    timestamp: ts,
                    doctype: 'dcor',
                    ct: output,
                    a: ats.a + '.dcor'
                }]
            });
        }
    },
    
    submitsessionCallback: function (rsp) {
        var me = (rsp.scope||this);
        
        me.gopreviousdashboardHandler(rsp);
        
        Ext.defer(function () {
            this.main.feedback.say({
                title: 'Success',
                message: 'Test Session saved.'
            })
        },
        1200,
        this)
    },
    
    buildSamples: function (sp) {
        var ats = this.main.data.getProvider("activetestsession");
        
    },
    
    /* -----------------------------------------------------------------------
    submitpostsupplementary:
    Saves the post supplementary report: Updates the test session status to COMPLETED; creates
    dcor document; Sends email to athlete; builds samples
    __________________________________________________________________________ */

    
    /* -----------------------------------------------------------------------
    startmanifest:
    Opens the mainfest form and displays waybill photo
    __________________________________________________________________________ */
    startmanifestHandler: function (sp) {
        var am = this.main.data.getProvider("activemanifest");
        
        var desc = this.main.data.getDescendants(am.ca);
        var parr = this.main.data.filter(desc, 'cid EQ document');
        
        var photo = this.main.data.getObjectByName('waybillphoto');
        var cbtn = this.main.data.getObjectByName('clearphotobtn');
           
        if (parr.length>0){
            photo.ct = parr[0].ct;
           cbtn.disabled = false;
        }
        
        this.main.actions.perform({
            cmd: 'goto',
            redirectsource: 'samplemanifestpanel',
            target: 'userinterface'
        });
        

    },
    
    
    /* -----------------------------------------------------------------------
    submitmanifest:
    Saves the Sample Manifest: creates manifest document; marks samples as 
    shipped;
    __________________________________________________________________________ */
    
    submitmanifestHandler: function (sp) {
        var ts = new Date();
        var am = this.main.data.getProvider("activemanifest");
        
        // Update the status of the manifest
        this.main.actions.perform({
            cmd: "put",
            component: this,
            objects: [am],
            ManifestStatus: 'COMPLETED'
        });

        // Get the template and apply the manifest form data to it
        var tpl = new Ext.XTemplate(this.main.data.getObjectByName('SampleManifestOutput').ct,this.main.util.getTemplateFunctions());
        var output = tpl.apply(am);

        
        this.main.data.create({
            callback: this.submitmanifestCallback,
            component: this,
            scope: this,
            data: [{
                cid: 'document',
                pa: am.a,
                l: 'Sample Manifest ' + Ext.Date.format(ts,'m-d-Y h:i A'),
                t: 0,
                timestamp: ts,
                doctype: 'samplemanifest',
                ct: output,
                a: am.a + '.samplemanifest'
            }]
        });
        
    },
    
    submitmanifestCallback: function (rsp) {
        var me = (rsp.scope||this);
        var am = me.main.data.getProvider("activemanifest");
        
        // bind the sample manifest document to the callback
        var doc = rsp.objects[0];
        var cb = Ext.bind(me.processsampleCallback,me,[doc],1);
           
        // Get an array of all the samples attached to the manifest
        var desc = me.main.data.getDescendants(am.ca);
        var uarr = me.main.data.filter(desc, 'cid EQ urinesample');
        var barr = this.main.data.filter(desc, 'cid EQ bloodsample');
        var sarr = uarr.concat(barr);
        
        for (var i=0,l=sarr.length;i<l;i++) {
            me.main.actions.perform({
                cmd: 'request',
                a: sarr[i].ca,
                relationship: 'item',
                callback: cb,
                scope: me,
                component: rsp.component
            });
        }
        
        me.gopreviousdashboardHandler(rsp);
    },
    
    processsampleCallback: function (rsp,doc) {
        var me = (rsp.scope||this);
        var sample = rsp.leaf;
        var ts = me.main.data.getParent(sample);
        var am = me.main.data.getProvider("activemanifest");
           
        me.main.actions.perform({
            cmd: 'put',
            component: me,
            objects: [sample],
            SampleStatus: 'SHIPPED',
            DateShipped: am.ShipmentDate
        });
        
        // link the sample manifest doc to the sample
        me.main.data.createLink({
            parent: sample,
            child: doc
        });
        
    },
    
    /* -----------------------------------------------------------------------
    saveusampleselection:
    Saves the selected Urine Samples to the sample manifest
    __________________________________________________________________________ */
    
    saveusampleselectionHandler: function (sp) {
        var am = this.main.data.getProvider("activemanifest");
        var auc = this.main.data.getProvider("activeurinecompleted");
        
//        debugger;
        
        // Link the selected sample to the sample manifest
        this.main.data.createLink({
            parent: am,
            child: auc
        });
        
        // Update the status of the sample
        this.main.actions.perform({
            cmd: "put",
            component: this,
            objects: [auc],
            SampleStatus: 'SELECTED'
        });
        
        // Refresh the grids
        this.main.util.get('completedurinesamples').loadData();
        this.main.util.get('urinesamplesgrid').loadData();
        
    },
    
    /* -----------------------------------------------------------------------
    removeselectedusample:
    Removes the selected Urine Samples from the sample manifest
    __________________________________________________________________________ */
    
    removeselectedusampleHandler: function (sp) {
    
        var uas = this.main.data.getProvider("activeurineselected");
        
        this.main.data.move({
            component: sp.component,
            scope: this,
            source: uas,
            destination: this.main.environment.get("RECYCLEBIN")
        });
        
        this.main.actions.perform({
            cmd: 'request',
            a: uas.ca,
            relationship: 'item',
            callback: this.removeselectedusampleCallback,
            scope: this,
            component: sp.component
        });
           
    },

    removeselectedusampleCallback: function (rsp) {
        var me = (rsp.scope||this);
           
        me.main.actions.perform({
            cmd: 'put',
            component: rsp,
            objects: [rsp.leaf],
            SampleStatus: 'AVAILABLE'
        });
        
      // Refresh the grids
        me.main.util.get('completedurinesamples').loadData();
        me.main.util.get('urinesamplesgrid').loadData();
    },
    
    
    /* -----------------------------------------------------------------------
    savebsampleselection:
    Saves the selected Blood Samples to the sample manifest
    __________________________________________________________________________ */
    
    savebsampleselectionHandler: function (sp) {
        var am = this.main.data.getProvider("activemanifest");
        var abc = this.main.data.getProvider("activebloodcompleted");
           
        // Link the selected sample to the sample manifest
        this.main.data.createLink({
            parent: am,
            child: abc
        });
        
        // Update the status of the sample
        this.main.actions.perform({
            cmd: "put",
            component: this,
            objects: [abc],
            SampleStatus: 'SELECTED'
        });
        
        // Refresh the grids
        this.main.util.get('completedbloodsamples').loadData();
        this.main.util.get('bloodsamplesgrid').loadData();
        
    },
    
    /* -----------------------------------------------------------------------
    removeselectedbsample:
    Removes the selected Blood Samples from the sample manifest
    __________________________________________________________________________ */
    
    removeselectedbsampleHandler: function (sp) {
    
//    debugger;
    
        var abs = this.main.data.getProvider("activebloodselected");
        
        this.main.data.move({
            component: sp.component,
            scope: this,
            source: abs,
            destination: this.main.environment.get("RECYCLEBIN")
        });
        
        this.main.actions.perform({
            cmd: 'request',
            a: abs.ca,
            relationship: 'item',
            callback: this.removeselectedbsampleCallback,
            scope: this,
            component: sp.component
        });
           
    },

    removeselectedbsampleCallback: function (rsp) {
        var me = (rsp.scope||this);
        
//        debugger;
        
        me.main.actions.perform({
            cmd: 'put',
            component: rsp,
            objects: [rsp.leaf],
            SampleStatus: 'AVAILABLE'
        });
        
      // Refresh the grids
        me.main.util.get('completedbloodsamples').loadData();
        me.main.util.get('bloodsamplesgrid').loadData();
    },
    
    /* -----------------------------------------------------------------------
    Waybill:
    Allows user to upload a photo of the waybill document for the sample manifest.
    User can take a photo of the waybill using the device camera, or select a
    photo from the photo library on the device
    __________________________________________________________________________ */
    
    selectwaybillphotoHandler: function (sp) {
        var cb = Ext.bind(this.onPhotoURISuccess,this);
//        debugger;
        navigator.camera.getPicture(cb, this.onPhotoFail, {
            quality: 50,
            destinationType: 0,
            encodingType: 0,
            targetWidth: 850,
            targetHeight: 1100,
            correctOrientation: false,
            sourceType: 0
         });
    },
    
    takewaybillphotoHandler: function () {
        var cb = Ext.bind(this.onPhotoURISuccess,this);
        navigator.camera.getPicture(cb, this.onPhotoFail, {
            quality: 50,
            destinationType: 0,
            encodingType: 0,
            targetWidth: 850,
            targetHeight: 1100,
            correctOrientation: false,
            sourceType: 1
        });
    },
    
    onPhotoURISuccess: function (dataURL) {

        this.photofile = dataURL;
        var htmldata = '<img src="data:image/jpeg;base64,' + dataURL + '" style="max-width:100%" />';
        var waybillphoto = this.main.util.get('waybillphoto');
        waybillphoto.ct = htmldata;
        
        waybillphoto.loadData();
        
        this.main.util.get('clearphotobtn').enable(true);
        this.main.util.get('uploadwaybillbtn').enable(true);
    },
    
    onPhotoFail: function (message) {
//        alert(message);
        this.fireEvent('cmd',{
            cmd: 'say',
            component: this,
            title: 'Photo',
            message: message
        });
    },
    
    deletephotoHandler: function (sp) {
        var waybill = this.main.util.get('waybillphoto');
        waybill.ct = '';
        waybill.loadData();
        
        this.main.util.get('clearphotobtn').disable(true);
        this.main.util.get('uploadwaybillbtn').disable(true);
    },
    
    uploadphotoHandler: function (sp) {
        var ts = new Date();
        var am = this.main.data.getProvider('activemanifest');
        var waybill = this.main.util.get('waybillphoto');

        this.main.actions.perform({
            cmd: 'setbusy',
            component: this.main.view
        });
        
        
        // Create waybill document
        this.main.data.create({
            callback: this.uploadphotoCallback,
            component: this,
            scope: this,
            data: [{
                cid: 'document',
                pa: am.a,
                l: 'Waybill ' + Ext.Date.format(ts,'m-d-Y h:i A'),
                t: 0,
                timestamp: ts,
                doctype: 'waybill',
                ct: waybill.ct,
                a: am.a + '.waybill'
            }]
        });
    },
    
    uploadphotoCallback: function (rsp) {
        // Create links to manifest
        var me = (rsp.scope||this);
        
        me.main.actions.perform({
            cmd: 'removebusy',
            component: me.main.view
        });
    },
    
    /* -----------------------------------------------------------------------
    Checkout/Checkin:
    Checkout or Checkin an In-Competition test session.  When a test is checkout
    by a DCO it becomes locked so that no other DCO can start the test.  Checkin
    returns the test session to the pool of available test sessions.
    __________________________________________________________________________ */
    
    checkoutHandler: function (sp) {
        var ts = new Date();
        
        var ats = this.main.data.getProvider("activetestsession");
        var status = ats.CheckedOut;
        var dconame = this.main.entity.getFullName();
        if ((!status)||(status==dconame)) {
            this.main.actions.perform({
                cmd: "put",
                component: this,
                objects: [ats],
//                CheckedOut: dconame,
                CheckedOut: 'tech',
                CheckedOutTS: ts
            });
            this.startdcfHandler(sp);
        } else {
            this.main.feedback.say({
                title: 'Error',
                message: 'This test session is in use.'
            });
        }
        
    },
    
    checkInActiveSessionHandler: function (sp) {
        var ts = new Date();
        
        var ats = this.main.data.getProvider("activetestsession");
//        this.main.actions.perform({
        
//        )
    },
    
    /* -----------------------------------------------------------------------
    Add Athlete:
    For In Competition Events opens a form so that the DCO can manually add an
    athletes information to the athlete queue.
    __________________________________________________________________________ */
    
    startAddAthleteHandler: function (sp) {
    
        var ae = this.main.data.getProvider("activeevent");
        
        // create the new site roster to add data to
        this.main.data.create({
            callback: this.startAthleteCallback,
            component: this,
            scope: this,
            data: [{
                cid: 'siteroster',
                pa: ae.a,
                l: 'New Site Roster',
                t: 0,
                RosterStatus: 'CREATED'
            }]
        });
    },
    
    startAthleteCallback: function (rsp) {
        var me = (rsp.scope||this);
        
        var ar = rsp.objects[0];
        me.main.data.setProvider("activeroster",ar.a);
        
        me.main.actions.perform({
            cmd: 'request',
            source: 'addathletepanel',
            target: 'userinterface',
            provider: ar.a
        });
    },

    /* -----------------------------------------------------------------------
    Add Selected Athlete to Queue:
    For In Competition Events allows the user to add a new athlete to the athlete
    pool by selecting the athlete from a list of all athletes.
    __________________________________________________________________________ */
    
    addSelectedAthleteToQueueHandler: function (sp) {
        var ath = this.main.data.getProvider("pickedathlete");
        
        var ae = this.main.data.getProvider("activeevent");
        
        // create the new site roster to add data to
        this.main.data.create({
            callback: this.addSelectedAthleteCallback,
            component: this,
            scope: this,
            data: [{
                cid: 'siteroster',
                pa: ae.a,
                l: 'Roster Entry - ' + ath.LastName,
                t: 0,
                RosterStatus: 'CREATED',
                FirstName: ath.FirstName,
                LastName: ath.LastName,
                Sport: ath.Sport,
                Discipline: ath.Discipline
            }]
        });
        
    },
    
    addSelectedAthleteCallback: function (rsp) {
        var me = (rsp.scope||this);
        
        var ar = rsp.objects[0];
        var ath = me.main.data.getProvider("pickedathlete");
        
        // Link the athlete to the site roster
        me.main.data.createLink({
            parent: ar,
            child: ath
        });
        
        me.main.actions.perform({
            cmd: 'request',
            source: 'icdashboard',
            target: 'userinterface'
        });
        
    },
    
    addAthleteToQueueHandler: function (sp) {
    
    },
    
    /* -----------------------------------------------------------------------
    Add Athletes to Roster:
    For In Competition Events adds the selected athletes to the site roster and
    creates new test sessions for the athletes.
    __________________________________________________________________________ */
    
    addAthletesToRosterHandler: function (sp) {
    
        var ts = new Date();
        var ae = this.main.data.getProvider("activeevent");

    
        // Find the selected athletes in the grid
        var agrid = this.main.util.get('athletespool');
        var grid = agrid.down("grid");
        var store = grid.getStore();
        var data = store.proxy.data;
        var atharray = [];
        
        // Add the selected athletes to an array
        for (var i=0,l=data.length;i<l;i++) {
           if (data[i].selectedathlete) {
               atharray.push(data[i]);
           }
        }
        
        if (atharray.length==0) {
            this.main.feedback.say({
                title: 'Add Selected to Site Roster',
                message: 'You must select at least one athlete from the athlete pool.'
            });
            return;
        }

        for (var i=0,l=atharray.length;i<l;i++){
            var ar = atharray[i];
            var desc = this.main.data.getDescendants(ar.ca);
            var arr = this.main.data.filter(desc, 'cid EQ athlete');
            var aa;
        
            // Check that athlete is linked to roster entry
            if (arr.length>0) {
                aa = arr[0];
            } else {
               this.fireEvent('cmd',{
                    cmd: 'say',
                    component: this,
                    title: 'Site Roster',
                    message: 'Error: roster entry not associated with a valid athlete.'
                });
                return;
            }
           
            // Update the Site roster entry for the athlete
            this.main.actions.perform({
                cmd: "put",
                component: this,
                objects: [ar],
                RosterStatus: 'OPENED',
                RosterHistory: 'Checked In',
                timestamp: ts
            });
           
           // Create a ledger entry to check athlete into site roster
            this.main.data.create({
                component: this,
                scope: this,
                data: [{
                    cid: 'ledgerentry',
                    l:  'Ledger Entry ' + Ext.Date.format(ts,'m-d-Y h:i A'),
                    pa: ar.a,
                    t: 0,
                    Timestamp: ts,
                    RosterHistory: 'Checked In'
                }]
            });
           
            var cb = Ext.bind(this.addTestSessionCallback,this,[aa],1);
        
            // Create a new test session
            this.main.data.create({
                callback: cb,
                component: this,
                scope: this,
                data: [{
                    cid: 'testsession',
                    l: 'Test Session ' + Ext.Date.format(ts,'m-d-Y h:i A'),
                    pa: ae.a,
                    t: 0,
                    ArrivalDate: ts,
                    ArrivalTime: ts,
                    EventID: ae.EventID,
                    TestingAuthority: 'USADA',
                    ResultsAuthority: 'USADA',
                    SiteID: ae.SiteID,
                    EventType: 'In-Competition',
                    designation: 'event',
                    Urine: 1,
                    UrineType: 'full',
                    Status: 'ASSIGNED',
                    CheckedOut: false
                }]
            });
        }
        
        this.gopreviousdashboardHandler();
    },
    
    addTestSessionCallback: function (rsp,aa) {
        
        var me = (rsp.scope||this);
        var ts = rsp.objects[0];
        
        var cb = Ext.bind(me.linkTestSessionCallback,me,[ts],1);

        // Request the athele object so we can link to this test session
        me.main.actions.perform({
            cmd: 'request',
            a: aa.ca,
            relationship: 'descendants',
            callback: cb,
            scope: me,
            component: rsp.component
        });
        
    },
        
    linkTestSessionCallback: function (rsp,ts) {
    
        var me = (rsp.scope||this);

//        debugger;
        // Link the athlete to the new test session record
        me.main.data.createLink({
            parent: ts,
            child: rsp.leaf
        });
        
        // Create the residence and mailing addresses if they exists
        var arr = me.main.data.filter(rsp.desc, 'cid EQ address');
        
        for (var i=0,l=arr.length;i<l;i++) {
            me.main.data.create({
                callback: Ext.emptyFn,
                component: rsp,
                scope: me,
                data: [{
                    cid: 'address',
                    l: 'Address',
                    pa: ts.ca,
                    t: 0,
                    AddressType: arr[i].AddressType||'',
                    Addr1: arr[i].Addr1||'',
                    Addr2: arr[i].Addr2||'',
                    City: arr[i].City||'',
                    State: arr[i].State||'',
                    Zip: arr[i].Zip||'',
                    Country: arr[i].Country||''
                }]
            });
        }
        
    },

    
    /* -----------------------------------------------------------------------
    Update person roster status:
    Toggles the roster status for the active support person and creates a new 
    roster entry.
    __________________________________________________________________________ */
    
    updatePersonRosterStatusHandler: function (sp) {
    
        var ap = this.main.data.getProvider("activeperson");
        
        var newstatus = (ap.RosterHistory=='Checked In') ? 'Checked Out' : 'Checked In';
        
        var cb = Ext.bind(this.updatePersonStatusCallback,this,sp.component,1);
        Ext.Msg.show({
            title: 'Update Site Roster Status',
            message: 'Update the Site Roster status for this person to ' + newstatus + '?',
            buttons: Ext.Msg.OKCANCEL,
            fn: cb
        });
    },
    
    updatePersonStatusCallback: function (rsp,cmp) {
        var me = (rsp.scope||this);
        var ap = me.main.data.getProvider("activeperson");
        
        if (rsp==='ok') {
            me.doUpdateRosterStatus(ap,cmp);
        }
        
    },
    
    /* -----------------------------------------------------------------------
    Update athlete roster status:
    Updates the site roster status for the athlete: toggles between Checked In/
    Checked Out; adds a new roster entry.
    __________________________________________________________________________ */
    
    updateAthleteRosterStatusHandler: function (sp) {
        var ar = this.main.data.getProvider("activesiteroster");
        
        var newstatus = (ar.RosterHistory=='Checked In') ? 'Checked Out' : 'Checked In';
        
        var tgrid = this.main.util.get('siterostergrid');
        var grid = tgrid.down("grid");

        
        var cb = Ext.bind(this.updateAthleteStatusCallback,this,[tgrid],1);
        Ext.Msg.show({
            title: 'Update Site Roster Status',
            message: 'Update the Site Roster status for this athlete to ' + newstatus + '?',
            buttons: Ext.Msg.OKCANCEL,
            fn: cb
        });
//        this.doUpdateRosterStatus(ar);
    },
    
    updateAthleteStatusCallback: function (rsp,cmp) {
        var me = (rsp.scope||this);
        var asr = me.main.data.getProvider("activesiteroster");
        
        if (rsp==='ok') {
           me.doUpdateRosterStatus(asr,cmp);
        }
    },
    
    doUpdateRosterStatus: function (rsp,cmp) {
        var me = (rsp.scope||this);
        
        var ts = new Date();
        var newstatus = (rsp.RosterHistory=='Checked In') ? 'Checked Out' : 'Checked In';
        
        me.main.actions.perform({
            cmd: "put",
            component: me,
            objects: [rsp],
            RosterHistory: newstatus
        });
       
        //Create a ledger entry for this person
        me.main.data.create({
            component: me,
            scope: me,
            data: [{
                cid: 'ledgerentry',
                l:  'Ledger Entry ' + Ext.Date.format(ts,'m-d-Y h:i A'),
                pa: rsp.a,
                t: 0,
                Timestamp: ts,
                RosterHistory: newstatus
            }]
        });
        
        var arr = me.main.render.getHistory();
        
        for (var i=arr.length,l=0;i>l;i--){
            var item = arr.pop();
            var src = item.source||item.redirectsource;
            me.main.actions.perform(item);
            break;
        }
           
        //        cmp.loadData();
        
//        me.main.actions.perform({
//            cmd: 'request',
//            source: 'icdashboard',
//            target: 'userinterface'
//        });
    },
    
    updateAthleteRosterEntourageHandler: function (sp) {
    
    },
    
    /* -----------------------------------------------------------------------
    Offer Assignment DCO:
    Enables RTL user to offer a test session assignment to the selected DCO:
    Checks AssignmentAPI for the test session status; Writes status update to
    AssignmentAPI; Reparents test session to DCO; Updates test session status.
    __________________________________________________________________________ */
    
    offerassignmentdcoHandler: function (sp) {
         
        // Get the DCO from the combobox
        var cbox = this.main.util.get('transferDCO');
        var aDCO = cbox.getRawValue();
        var dcoID = cbox.findRecordByDisplay(aDCO).data.value;
        
        // Find the selected test sessions in the grid
        var tgrid = this.main.util.get('rtlunassignedgrid');
        var grid = tgrid.down("grid");
        var store = grid.getStore();
        var data = store.proxy.data;
        var tarray = [{'DCO':aDCO,'DCOId':dcoID}];
        var tlist='';
        
        for (var i=0,l=data.length;i<l;i++) {
           if (data[i].TransferTestSession) {
               tarray.push(data[i]);
           }
        }
        
        if (tarray.length==1) {
            this.main.feedback.say({
                title: 'Offer Assignment',
                message: 'You must select at least one test session assignment.'
            });
            return;
        }
        
        for (var i=1,l=tarray.length;i<l;i++) {
            tlist = (tlist=='') ? tarray[i].AthletePrintedName : tlist + '<br>' + tarray[i].AthletePrintedName;
        }
        
        var cb = Ext.bind(this.doofferassignmentcheck,this,[tarray],1);
        // Verify selection with user
        Ext.Msg.show({
            title: 'Offer Assignment',
            message: 'The following assignments will be offered to ' + aDCO + ':<br>' + tlist,
            buttons: Ext.Msg.OKCANCEL,
            fn: cb
        });
        
    },
    
    doofferassignmentcheck: function (rsp,tdata) {
        var me = (rsp.scope||this);
        
        if (rsp==='ok') {
//            debugger;

            var DCOID = tdata[0].DCOId;

            for (var i=1,l=tdata.length;i<l;i++){
           
                var tsid = tdata[i].TSID;
                // bind callback function, attaching the test session object as
                // an additional argument
                var cb = Ext.bind(me.doofferassignmentCallback,me,tdata[i],1);
           
                // TESTING
                var rval = {"applicationid":"testsessionassignment","type":"success","message":"","data":[{"testsessionid":"8675309","dcoid":"1","statusid":"9","status":"Offered","statusdate":"today"}]};
                me.doofferassignmentCallback(rval,[tdata[0],tdata[i]]);
           
                // write test session assignment to API
//                me.main.actions.perform({
//                    cmd: "task",
//                    n: "testsessionassignment",
//                    dcoid: DCOID,
//                    statusid: "9",
//                    testsessionid: tsid,
//                    callback: cb,
//                    scope: me
//                });
            }
           
            me.main.actions.perform({
                cmd: 'request',
                source: 'icdashboard',
                target: 'userinterface'
            });
           
        }
    },
    
    doofferassignmentCallback: function (rsp,tdata) {
        var me = (rsp.scope||this);
        
        var ts = new Date();
        
        // Update the status and reset the parent
        if (rsp.type=="success") {
            var pdco = 'dco.' + tdata[0].DCOId;
            me.main.actions.perform({
                cmd: "put",
                component: me,
                objects: [tdata[1]],
                Status: 'OFFERED',
                OfferedDt: ts,
                pa: pdco
            });
        }

    },
    
    /* -----------------------------------------------------------------------
    Transfer Assignment DCO:
    Enables RTL user to transfer a test session assignment to the selected region:
    Checks AssignmentAPI for the test session status; Writes status update to
    AssignmentAPI; Reparents test session to region; Updates test session status.
    __________________________________________________________________________ */
    
    transferassignmentdcoHandler: function (sp) {
         
        // Get the DCO from the combobox
        var cbox = this.main.util.get('transferRegion');
        var aRegion = cbox.getRawValue();
        var regionID = cbox.findRecordByDisplay(aRegion).data.value;
        
        // Find the selected test sessions in the grid
        var tgrid = this.main.util.get('rtlunassignedgrid');
        var grid = tgrid.down("grid");
        var store = grid.getStore();
        var data = store.proxy.data;
        var tarray = [{'Region':aRegion,'RegionId':regionID}];
        var tlist='';
        
        for (var i=0,l=data.length;i<l;i++) {
           if (data[i].TransferTestSession) {
               tarray.push(data[i]);
           }
        }
        
        if (tarray.length==1) {
            this.main.feedback.say({
                title: 'Transfer Assignment',
                message: 'You must select at least one test session assignment.'
            });
            return;
        }
        
        for (var i=1,l=tarray.length;i<l;i++) {
            tlist = (tlist=='') ? tarray[i].AthletePrintedName : tlist + '<br>' + tarray[i].AthletePrintedName;
        }
        
        var cb = Ext.bind(this.dotransferassignmentcheck,this,[tarray],1);
        // Verify selection with user
        Ext.Msg.show({
            title: 'Transfer Assignment',
            message: 'The following assignments will be transferred to ' + aRegion + ' Region:<br>' + tlist,
            buttons: Ext.Msg.OKCANCEL,
            fn: cb
        });
        
    },
    
    dotransferassignmentcheck: function (rsp,tdata) {
        var me = (rsp.scope||this);
        
        if (rsp==='ok') {
//            debugger;

            var RegionID = tdata[0].RegionId;

            for (var i=1,l=tdata.length;i<l;i++){
           
                var tsid = tdata[i].TSID;
                // bind callback function, attaching the test session object as
                // an additional argument
                var cb = Ext.bind(me.dotransferassignmentCallback,me,tdata[i],1);
           
                // TESTING
                var rval = {"applicationid":"testsessionassignment","type":"success","message":"","data":[{"testsessionid":"8675309","dcoid":"1","statusid":"9","status":"Offered","statusdate":"today"}]};
                me.dotransferassignmentCallback(rval,[tdata[0],tdata[i]]);
           
                // write test session assignment to API
//                me.main.actions.perform({
//                    cmd: "task",
//                    n: "testsessionassignment",
//                    regionid: RegionID,
//                    statusid: "11",
//                    testsessionid: tsid,
//                    callback: cb,
//                    scope: me
//                });
            }
           
            me.main.actions.perform({
                cmd: 'request',
                source: 'rtlassignmentsdash',
                target: 'userinterface'
            });
           
        }
    },
    
    dotransferassignmentCallback: function (rsp,tdata) {
        var me = (rsp.scope||this);
        
        var ts = new Date();
        
        // Update the status and reset the parent
        if (rsp.type=="success") {
            var pregion = 'region.' + tdata[0].RegionId;
            me.main.actions.perform({
                cmd: "put",
                component: me,
                objects: [tdata[1]],
                Status: 'UNASSIGNED',
                pa: pregion
            });
        }

    },
    
    /* -----------------------------------------------------------------------
    Accept Assignment:
    Enables DCO user to accept one or more offered assignments: Checks 
    AssignmentAPI for test session status; Writes status update to AssignmentAPI;
    Changes status of test session.
    __________________________________________________________________________ */
    
    acceptassignmentsHandler: function (sp) {
        
        // Find the selected test sessions in the grid
        var tgrid = this.main.util.get('offeredassignmentsgrid');
        var grid = tgrid.down("grid");
        var store = grid.getStore();
        var data = store.proxy.data;
        var tarray = [];
        var tlist='';
        
        // Find the selected items in the grid
        for (var i=0,l=data.length;i<l;i++) {
           if (data[i].SelectedTestSession) {
               tarray.push(data[i]);
           }
        }
        
        // Check that something is selected
        if (tarray.length==0) {
            this.main.feedback.say({
                title: 'Accept Assignment',
                message: 'You must select at least one test session assignment'
            });
            return;
        }
        
        // format for verification
        for (var i=0,l=tarray.length;i<l;i++) {
            tlist = (tlist=='') ? tarray[i].AthletePrintedName : tlist + '<br>' + tarray[i].AthletePrintedName;
        }
        
        // Include the selected test sessions in the callback
        var cb = Ext.bind(this.acceptassignmentscheck,this,[tarray],1);
        
        // Verify selection with user
        Ext.Msg.show({
            title: 'Accept Assignments',
            message: 'The following assignments will be accepted:<br>' + tlist,
            buttons: Ext.Msg.OKCANCEL,
            fn: cb
        });
    },
    
    acceptassignmentscheck: function (rsp,tdata) {
        var me = (rsp.scope||this);
    
        if (rsp==='ok') {
            for (var i=0,l=tdata.length;i<l;i++){
                var tsid = tdata[i].TSID;
                me.doacceptassignmentHandler(tdata[i]);
            }
        }
        me.assignmentsdashboardHandler();
    },
    
    acceptassignmentHandler: function (sp) {
        var aa = this.main.data.getProvider("activeassignment");
        this.doacceptassignmentHandler(aa);
        this.assignmentsdashboardHandler();
    },
    
    doacceptassignmentHandler: function (rsp) {
    
        var me = (rsp.scope||this);

        // Dummy up return value until assignment is working on server
//        var rval = {"applicationid":"testsessionassignment","type":"success","message":"","data":[{"testsessionid":"8675309","dcoid":"1","statusid":"1","status":"Accepted","statusdate":"today"}]};
//        this.acceptassignmentCallback(rval,[rsp]);

//        debugger;
        
        // DISABLED UNTIL ASSIGNMENT TASK IS WORKING ON SERVER
        var cb = Ext.bind(me.acceptassignmentCallback,me,[rsp],1);
        
        // Write assignment status to API
        this.main.actions.perform({
            cmd: "task",
            n: "testsessionassignment",
            action: '/api/write',
            testsessionid: rsp.TSID,
            dcoid: 6||this.entity.getUser().DCOID,
            statusid: 1,
            callback: cb,
            scope: this
        });
    },
    
    acceptassignmentCallback: function (rsp, atest) {
        var me = (rsp.scope||this);
        
//        debugger;
           
        switch (rsp.success) {
            case "warning":
            case "success":
                me.main.actions.perform({
                    cmd: "put",
                    component: me,
                    objects: [atest[0]],
                    Status: 'ASSIGNED'
                });
                break;
           case "error":
               var msgtitle = 'Error';
               var msgmessage = 'An error has occurred accepting assignment.';
               if ((Ext.isArray(rsp.events)) && (rsp.events.length>0)) {
                   msgtitle = rsp.events[0].Message;
                   msgmessage = rsp.events[0].message;
               }
               this.main.feedback.say({
                   title: msgtitle,
                   message: msgmessage
               });
               break;
        }
    },
    
    assignmentsdashboardHandler: function (sp) {

        this.main.actions.perform({
            cmd: 'request',
            source: 'assignmentsdash',
            target: 'userinterface'
        });
    },
    
    declineassignmentHandler: function (sp) {
        var aa = this.main.data.getProvider("activeassignment");
        
    },

    /* -----------------------------------------------------------------------
    Load Whereabouts:
    Opens a new browser window and displays whereabouts info for the selected
    athlete.
    __________________________________________________________________________ */
    
    loadwhereaboutsHandler: function () {

        this.main.actions.perform({
            cmd: "task",
            n: "whereabouts",
            dcoid: 6||this.entity.getUser().DCOID,
            callback: this.loadwhereaboutsCallback,
            scope: this
        });

    },

loadwhereaboutsCallback: function (rsp) {
    var me = (rsp.scope||this);

        //TODO:  need to add athlete id to this url per documentation.
        var url = "https://athlete-admin-staging.usada.org/Whereabouts/dco/forathlete?logintoken=" + rsp.events[0].token + "&athleteid=55299";
        
        //Open in a new browser window in Safari.
        window.open(url, "_system");

},
    
    
    
     /* -----------------------------------------------------------------------
    Business Rules.  Checks a test session to see if it is complete.
    __________________________________________________________________________ */
    
    
    checktestsessionHandler: function (sp) {
    
        //This function is called after rule's component (DCF tabpanel) is added to the screen.
        //It is also called every time the completed context changes from any of the managers in the DCF section.
        
        this.main.actions.perform({
            cmd: 'setbusy',
            component: sp.component,
            message: 'Checking progress...'
        });
        
        //Important: Defer to allow all components to draw on screen, then must set up all managers in
        //manageProviders before this function will work correctly.
        
        /* Common pitfalls so far:
            incorrect provider
            provider must be character exact
            must separate providers and managers into trees so they do not overlap.
            provider name is case sensitive and must be exact
            forgetting to put required:true on a component
            
        */

        Ext.defer(function () {
        
            var obj = {
                component: sp.component,
                subtopic: (sp.rule) ? sp.rule.value : ''
            }
            
            //container = name of the component you want to mark with complete/incomplete style.
            //manager = name of a manager component that is managing a section of fields and marking them complete/incomplete
            //title = base title to alter with complete/incomplete style.
            //required = if true it will ignore started setting and track the completeness of that section no matter what.
            
            var ts = this.main.data.getProvider("{activetestsession}");
            var ath = this.main.data.getProvider("{activetestsession > athlete[0]}");

            
            //DCF
            //--------------------------------
            this.checkForm(Ext.apply({dataprovider:ts,title:'DCF',container:'dcfLandingForm'},obj));
            
            
            
            //Notification Form
            //-------------------------------
            this.checkForm(Ext.apply({dataprovider:ts,title:'Notification',container: 'dcornot',completed:[ath.athletenameFieldset_completed,ts.notificationFieldset_completed,ts.chaperoneFS_completed]},obj));
            
            
            //Part 1 (Notification)
            this.checkSection(Ext.apply({title: "Notification",container: "notificationPart1",managers: ["athletenameFieldset","notificationFieldset","chaperoneFS"],required: true},obj));
            
            //Part 2 (Regulations)
            this.checkSection(Ext.apply({title: "Regulations",container: "notificationPart2"},obj));
            
            //Part 3 (Notification Letter)
            this.checkSection(Ext.apply({title: "Notification Letter",container: "notificationPart3"},obj));
           
            //Part 4 (Signature)
            this.checkSection(Ext.apply({title: "Signature", container: "notificationPart4", managers: ["notificationPart4"],required:true},obj));
    


            //Athlete Info Form
            //-------------------------------
            this.checkForm(Ext.apply({dataprovider:ts,title:'Athlete<br/>Info',container: 'dcorai',completed:[ts.athleteinfoPart1_completed,ath.dobFieldset_completed,ath.nationalityFS_completed,ts.residenceAddressFS_completed,ts.mailingFS_completed]},obj));
            
            
            //Part 1 (Arrival)
            this.checkSection(Ext.apply({title: "Arrival", container: "athleteinfoPart1", managers: ["athleteinfoPart1"],required:true},obj));
            
            //Part 2 (Identification)
            this.checkSection(Ext.apply({title: "Identification",container: "athleteinfoPart2", managers: ["dobFieldset","nationalityFS"],required:true},obj));
        
            //Part 3 (Residence Address)
            this.checkSection(Ext.apply({title: "Residence Address",container: "athleteInfoPart3", managers: ["residenceAddressFS"],required:true},obj));
            
            //Part 4 (Mailing Address)
            this.checkSection(Ext.apply({title: "Mailing Address",container: "athleteInfoPart4", managers: ["mailingFS"],required:true},obj));
            
            //Part 5 (Support Personnel)
            this.checkSection(Ext.apply({title: "Support Personnel",container: "athleteInfoPart5"},obj));


            //Lab/Sample Info Form
            //-------------------------------
            this.checkForm(Ext.apply({dataprovider:ts,title:'Lab/Sample<br/>Info',container: 'dcorlab',completed:[ts.labSampleInfoPart1_completed,ath.genderFS_completed,ts.labSampleInfoPart3_completed]},obj));
            
            //Part 1 (Competition)
            this.checkSection(Ext.apply({title: "Competition",container: "labSampleInfoPart1",managers: ["labSampleInfoPart1","genderFS"],required: true},obj));
            
            //Part 2 (Tests)
            this.checkSection(Ext.apply({title: "Tests",container: "labSampleInfoPart2"},obj));
            
            //Part 3 (Site)
            this.checkSection(Ext.apply({title: "Site",container: "labSampleInfoPart3",managers: ["labSampleInfoPart3"],required: true},obj));


            //Blood Sample Form
            //-------------------------------
            
            
            //Need to loop over bloodsamples.
            var bcomplete = true;
            var bfound = false;
            var bsamples = this.main.data.filter(this.main.data.getChildren(ts),"cid EQ bloodsample");
            for (var i=0,l=bsamples.length;i<l;i++) {
                bfound = true;
                var b = bsamples[i];
                if (b.BloodSamplePart3_completed) {
                } else {
                    bcomplete = false;
                }
            }
            
            //Recheck Blood Samples list.
            //Might need to move this into a rule that only works when Blood data is being added.
            var dvw = sp.component.down("xdataview[name='BloodSamplesList']");
            if (dvw) dvw.refresh();

            if (ts.Blood!=true){
                this.checkForm(Ext.apply({dataprovider:ts,title: "Blood<br/>Sample",container: "dcorbl",disabled: true},obj));
            } else {
                if (bfound) {
                    this.checkForm(Ext.apply({dataprovider:ts,title: "Blood<br/>Sample",container: "dcorbl",completed: [bcomplete,b.BloodSamplePart4_completed,b.BloodSamplePart5_completed]},obj));
                } else {
                    this.checkForm(Ext.apply({dataprovider:ts,title: "Blood<br/>Sample",container: "dcorbl", completed: [false]},obj));
                }
            }
            
            //Part 1 (Training/Blood Loss)
            this.checkSection(Ext.apply({title: "Training/Blood Loss",container: "BloodSamplePart1",required: true},obj));
            
            //Part 2 (Whereabouts/Altitude)
            this.checkSection(Ext.apply({title: "Whereabouts/Altitude",container: "BloodSamplePart2",required: true},obj));
            
            //Part 3 (Sample Info)
            if (bfound) {
                this.setComplete(Ext.apply({title: "Sample Info",container: "BloodSamplePart3", completed: bcomplete},obj));
            } else {
                this.checkSection(Ext.apply({title: "Sample Info",container: "BloodSamplePart3",required: true},obj));
            }
            
            //Part 4 (BCO Signature)
            if (bfound) {
                this.checkSection(Ext.apply({title: "BCO Signature",container: "BloodSamplePart4",managers:["BloodSamplePart4"],required: true},obj));
            } else {
                this.checkSection(Ext.apply({title: "BCO Signature",container: "BloodSamplePart4",required: true},obj));
            }
            
            //Part 5 (DCO Signature)
            if (bfound) {
                this.checkSection(Ext.apply({title: "DCO Signature",container: "BloodSamplePart5",managers:["BloodSamplePart5"],required: true},obj));
            } else {
                this.checkSection(Ext.apply({title: "DCO Signature",container: "BloodSamplePart5",required: true},obj));
            }
            


            //Urine Sample Form
            //-------------------------------
            
            //Need to loop over urinesamples.
            var ucomplete = true;
            var ufound = false;
            var usamples = this.main.data.filter(this.main.data.getChildren(ts),"cid EQ urinesample");
            for (var i=0,l=usamples.length;i<l;i++) {
                ufound = true;
                var u = usamples[i];
                if (u.ChaperoneForm_completed && u.urinesampleform_completed) {
                } else {
                    ucomplete = false;
                }
            }
        
            if (ts.Urine!=true) {
                this.checkForm(Ext.apply({dataprovider:ts,title:'Urine<br/>Sample',container: 'dcorur',disabled: true},obj));
            } else {
                if (ufound) {
                    this.checkForm(Ext.apply({dataprovider:ts,title:'Urine<br/>Sample',container: 'dcorur',completed:[ucomplete]},obj));
                } else {
                    this.checkForm(Ext.apply({dataprovider:ts,title:'Urine<br/>Sample',container: 'dcorur',completed:[false]},obj));
                }
            }
            
            //Recheck Urine Samples list.
            //Might need to move this into a rule that only works when data is being added.
            var dvw = sp.component.down("xdataview[name='urinesamples']");
            if (dvw) dvw.refresh();
            
                  
            //Part 1 (Chaperone)
            if (ufound) {
                this.checkSection(Ext.apply({title: "Chaperone",container: "ChaperoneForm",managers:["ChaperoneForm"],required: true},obj));
            } else {
                this.checkSection(Ext.apply({title: "Chaperone",container: "ChaperoneForm",required: true},obj));
            }
            
            //Part 2 (Urine Sample)
            if (ufound) {
                this.checkSection(Ext.apply({title: "Urine Sample",container: "urinesampleform",managers:["urinesampleform"],required: true},obj));
            } else {
                this.checkSection(Ext.apply({title: "Urine Sample",container: "urinesampleform",required: true},obj));
            }


            //Research Consent Form
            //-------------------------------
            this.checkForm(Ext.apply({dataprovider:ts,title:'Research<br/>Consent',container: 'dcorcs',completed:[ts.ResearchConsentPart1_completed,ts.ResearchConsentPart2_completed]},obj));
            
            
            //Part 1 (Accept Terms)
            this.checkSection(Ext.apply({title: "Accept Terms",container: "ResearchConsentPart1",managers: ["ResearchConsentPart1"],required: true},obj));
            
            //Part 2 (Athlete Signature)
            this.checkSection(Ext.apply({title: "Athlete Signature",container: "ResearchConsentPart2",managers: ["ResearchConsentPart2"],required: true},obj));


            //Declaration of Use Form
            //-------------------------------
            this.checkForm(Ext.apply({dataprovider:ts,title:'Declaration<br/>Of Use',container: 'dcordou',completed:[ts.douDeclarations_completed,ts.douAgreement_completed]},obj));
            
            //Part 1 (Declarations)
            this.checkSection(Ext.apply({title: "Declarations",container: "douDeclarations",managers: ["douDeclarations"],required: true},obj));
            
            //Part 2 (Laboratory/Sample Information)
            this.checkSection(Ext.apply({title: "Laboratory/Sample Information",container: "douLabSampleInfo",required: true},obj));
            
            //Part 3 (Agreement)
            this.checkSection(Ext.apply({title: "Agreement",container: "douAgreement",managers: ["douAgreement"],required: true},obj));



            //Partial Sample Form
            //-------------------------------
            
            //Need to loop over partial samples.
            var pscomplete = true;
            var partials = this.main.data.filter(this.main.data.getChildren(ts),"cid EQ partialsample");
            for (var i=0,l=partials.length;i<l;i++) {
                var p = partials[i];
                if (p.dcorpsVolumeKit_completed && p.dcorpsDateTimeForm_completed && p.dcorpsWitnessSig_completed && p.dcorpsDCOSig_completed && p.dcorpsDCOSig_completed && p.dcorpsDateTimeOpened_completed && p.dcorpsAthleteSig_completed && p.dcorpsDCOSig2_completed) {
                } else {
                    pscomplete = false;
                }
            }
            
            //Recheck Partial Samples list.
            //Might need to move this into a rule that only works when data is being added.
            var dvw = sp.component.down("xdataview[name='PartialSampleList']");
            if (dvw) dvw.refresh();
            
            this.checkForm(Ext.apply({dataprovider:ts,title:'Partial<br/>Samples',container: 'dcorpt',completed:[pscomplete]},obj));
            
            
            //Part 1 (Volume/Kit)
            this.checkSection(Ext.apply({title: "Volume/Kit",container: "dcorpsVolumeKit",managers: ["dcorpsVolumeKit"],required: true},obj));
            
            //Part 2 (Date/Time Sealed)
            this.checkSection(Ext.apply({title: "Date/Time Sealed",container: "dcorpsDateTimeForm",managers:["dcorpsDateTimeForm"],required: true},obj));
            
            //Part 3 (Witness Signature)
            this.checkSection(Ext.apply({title: "Witness Signature",container: "dcorpsWitnessSig",managers: ["dcorpsWitnessSig"],required: true},obj));
            
            //Part 4 (DCO Signature)
            this.checkSection(Ext.apply({title: "DCO Signature",container: "dcorpsDCOSig",managers: ["dcorpsDCOSig"],required: true},obj));
            
            //Part 5 (Date/Time Opened)
            this.checkSection(Ext.apply({title: "Date/Time Opened",container: "dcorpsDateTimeOpened",managers: ["dcorpsDateTimeOpened"],required: true},obj));
            
            //Part 6 (Athlete Signature)
            this.checkSection(Ext.apply({title: "Athlete Signature",container: "dcorpsAthleteSig",managers: ["dcorpsAthleteSig"],required: true},obj));
            
            //Part 7 (DCO Signature)
            this.checkSection(Ext.apply({title: "DCO Signature",container: "dcorpsDCOSig2",managers: ["dcorpsDCOSig2"],required: true},obj));
            


            //Custody Transfer Form
            //-------------------------------
            
            //Need to loop over custody transfers.
            var ctcomplete = true;
            var transfers = this.main.data.filter(this.main.data.getChildren(ts),"cid EQ custodytransfer");
            for (var i=0,l=transfers.length;i<l;i++) {
                var ct = transfers[i];
                if (ct.reasonForm_completed && ct.AthleteSig2_completed && ct.DcoSigForm_completed  && ct.TransferAthleteForm_completed && ct.AthleteSigForm_completed && ct.DcoSig2_completed) {
                } else {
                    ctcomplete = false;
                }
            }
            
            //Recheck Custody Transfer form.
            //Might need to move this into a rule that only works when data is being added.
            var dvw = sp.component.down("xdataview[name='custodytransferlist']");
            if (dvw) dvw.refresh();
            
//            this.checkForm(Ext.apply({dataprovider:ts,title:'Custody<br/>Transfer',container: 'dcortrans',completed:[ctcomplete]},obj));
            this.checkForm(Ext.apply({dataprovider:ts,title:'Custody<br/>Transfer',container: 'dcortrans',completed:[true]},obj));

            
                  
            //Part 1 (Reason for Transfer)
            this.checkSection(Ext.apply({title: "Reason For Transfer",container: "reasonForm",managers: ["reasonForm"],required: true},obj));
            
            //Part 2 (Athlete Signature)
            this.checkSection(Ext.apply({title: "Athlete Signature",container: "AthleteSig2",managers:["AthleteSig2"],required: true},obj));
            
            //Part 3 (DCO Signature)
            this.checkSection(Ext.apply({title: "DCO Signature",container: "DcoSigForm",managers: ["DcoSigForm"],required: true},obj));
            
            //Part 4 (Transfer to Athlete)
            this.checkSection(Ext.apply({title: "Transfer to Athlete",container: "TransferAthleteForm",managers: ["TransferAthleteForm"],required: true},obj));
            
            //Part 5 (Athlete Signature)
            this.checkSection(Ext.apply({title: "Athlete Signature",container: "AthleteSigForm",managers: ["AthleteSigForm"],required: true},obj));
            
            //Part 6 (DCO Signature)
            this.checkSection(Ext.apply({title: "DCO Signature",container: "DcoSig2",managers: ["DcoSig2"],required: true},obj));
            


            //Supplementary Form
            //-------------------------------
            
            //Need to loop over custody transfers.
            var supcomplete = true;
            var supplementaries = this.main.data.filter(this.main.data.getChildren(ts),"cid EQ supplementaryreport");
            for (var i=0,l=supplementaries.length;i<l;i++) {
                var sup = supplementaries[i];
                if (sup.dcorsupPart2_completed && sup.dcorsupPart5_completed) {
                } else {
                    supcomplete = false;
                }
            }
            
            //Recheck Supplementary form.
            //Might need to move this into a rule that only works when data is being added.
            var dvw = sp.component.down("xdataview[name='addsupplementarylist']");
            if (dvw) dvw.refresh();
            
            this.checkForm(Ext.apply({dataprovider:ts,title:'Supplementary<br/>Report',container: 'dcorsup',completed:[supcomplete]},obj));
            
                  
            //Part 1 (Applies To)
            this.checkSection(Ext.apply({title: "Applies To",container: "dcorsupPart1",required: true},obj));
            
            //Part 2 (Person Submitting)
            this.checkSection(Ext.apply({title: "Person Submitting",container: "dcorsupPart2",managers:["dcorsupPart2"],required: true},obj));
            
            //Part 3 (Purpose of Report)
            this.checkSection(Ext.apply({title: "Purpose of Report",container: "dcorsupPart3",required: true},obj));
            
            //Part 4 (Comments)
            this.checkSection(Ext.apply({title: "Comments",container: "dcorsupPart4",required: true},obj));
            
            //Part 5 (Signature)
            this.checkSection(Ext.apply({title: "Signature",container: "dcorsupPart5",managers: ["dcorsupPart5"],required: true},obj));
            
            
           

            //Signatures Form
            //-------------------------------
            
            this.checkForm(Ext.apply({dataprovider:ts,title:'Signatures',container: 'dcorsig',completed:[ts.signaturesPart2_completed,ts.signaturesPart7_completed]},obj));
           
                  
            //Part 1 (Review)
            this.checkSection(Ext.apply({title: "Review", container: "signaturesPart1", required:true},obj));
            
            //Part 2 (Athlete Signature)
            this.checkSection(Ext.apply({title: "Athlete<br/>Signature",container: "signaturesPart2", managers: ["signaturesPart2"],required:true},obj));
        
            //Part 3 (Representative Signature)
            this.checkSection(Ext.apply({title: "Representative<br/>Signature",container: "signaturesPart3"},obj));
            
            //Part 4 (Language Specialist)
            this.checkSection(Ext.apply({title: "Language<br/>Specialist",container: "signaturesPart4"},obj));
            
            //Part 5 (Other Signatures)
            this.checkSection(Ext.apply({title: "Other<br/>Signatures",container: "signaturesPart5"},obj));
            
            //Part 6 (Subsequent Officer)
            this.checkSection(Ext.apply({title: "Subsequent<br/>Officer",container: "signaturesPart6"},obj));
            
            //Part 7 (DCO Signature)
            this.checkSection(Ext.apply({title: "DCO<br/>Signature",container: "signaturesPart7", managers: ["signaturesPart7"],required:true},obj));
            
            //Part 8 (DCOR Receipt)
            this.checkSection(Ext.apply({title: "DCOR<br/>Receipt",container: "signaturesPart8"},obj));
            
            //Part 9 (Submit Forms)
            this.checkSection(Ext.apply({title: "Submit<br/>Forms",container: "signaturesPart9"},obj));
            
            this.checkCollection(Ext.apply({button:"submittestsessionbutton",completed:[ts.dcornot_completed,ts.dcorai_completed,ts.dcorlab_completed,ts.dcorbl_completed,ts.dcorur_completed,ts.dcorcs_completed,ts.dcordou_completed,ts.dcorpt_completed,ts.dcortrans_completed,ts.dcorsup_completed,ts.dcorsig_completed]},obj));
            
            //Done
            this.main.actions.perform({
                cmd: 'removebusy',
                component: sp.component
            });
            
        },1000,this);

    },
    
    
     checkuarHandler: function (sp) {
    
        //This function is called after rule's component (UAR tabpanel) is added to the screen.
        //It is also called every time the completed context changes from any of the managers in the UAR section.
        
        this.main.actions.perform({
            cmd: 'setbusy',
            component: sp.component,
            message: 'Checking progress...'
        });
        
        //Important: Defer to allow all components to draw on screen, then must set up all managers in
        //manageProviders before this function will work correctly.
        
        /* Common pitfalls so far:
            incorrect provider
            provider must be character exact
            must separate providers and managers into trees so they do not overlap.
            provider name is case sensitive and must be exact
            forgetting to put required:true on a component
            
        */

        Ext.defer(function () {
        
            var obj = {
                component: sp.component,
                subtopic: (sp.rule) ? sp.rule.value : ''
            }
            
            //container = name of the component you want to mark with complete/incomplete style.
            //manager = name of a manager component that is managing a section of fields and marking them complete/incomplete
            //title = base title to alter with complete/incomplete style.
            //required = if true it will ignore started setting and track the completeness of that section no matter what.
            
            var uar = this.main.data.getProvider("{activeuar}");
            var loc = this.main.data.getProvider("{activeuar > location}");

            
            //Athlete Information
            //--------------------------------
            this.checkForm(Ext.apply({dataprovider:uar,title:'Athlete<br>Information',container:'uarathleteinfoform',completed:[uar.uarathleteinfoform_completed]},obj));
            

            //Attempt Information
            //--------------------------------
            this.checkForm(Ext.apply({dataprovider:uar,title:'Attempt<br>Information',container:'attemptinfoform',completed:[uar.attemptinfoform_completed]},obj));
            
            //First Location
            //-------------------------------
            
            //Need to loop over Phone Calls.
            var pcomplete = true;
            if (uar.FirstCallsPlaced) {
                var pnumbers = this.main.data.filter(this.main.data.getChildren(uar),"cid EQ phonecall");
                for (var i=0,l=pnumbers.length;i<l;i++) {
                    var p = pnumbers[i];
                    if (p.UARLocation=='FirstLocation'){
                        if (p.FirstLocNumbersCalledForm_completed) {
                        } else {
                            pcomplete = false;
                        }
                    }
                }
            }
            
            //Recheck Phone Calls list.
            //Might need to move this into a rule that only works when Blood data is being added.
            var dvw = sp.component.down("xdataview[name='uarfirstnumberscalledlist']");
            if (dvw) dvw.refresh();
            
            
            this.checkForm(Ext.apply({dataprovider:uar,title: "First Location",container: "FirstLocationTabPanel",completed: [pcomplete,uar.FirstLocationLocationForm_completed,uar.FirstLocationTimeForm_completed,uar.FirstLocationCommentsForm_completed]},obj));
            
            //Location
            this.checkSection(Ext.apply({title: "Location",container: "FirstLocationLocationForm",managers:["FirstLocationLocationForm"],required: true},obj));
            
            //Time
            this.checkSection(Ext.apply({title: "Time",container: "FirstLocationTimeForm",managers:["FirstLocationTimeForm"],required: true},obj));
            
            //Person Contacted
            this.checkSection(Ext.apply({title: "Person Contacted",container: "FirstLocationPersonContactedForm",required: true},obj));
            
            //Restricted Access
            this.checkSection(Ext.apply({title: "Restricted Access",container: "FirstLocationRestrictedAccessForm",required: true},obj));
            
            if (uar.FirstCallsPlaced) {
            //Numbers Called
                this.setComplete(Ext.apply({title: "Numbers Called",container: "FirstLocNumbersCalledForm",completed: pcomplete},obj));
            } else {
                this.checkSection(Ext.apply({title: "Numbers Called",container: "FirstLocNumbersCalledForm",required: true},obj));
            }
            
            //Comments
            this.checkSection(Ext.apply({title: "Comments",container: "FirstLocationCommentsForm",managers:["FirstLocationCommentsForm"],required: true},obj));
            
            
            
            //Second Location
            //-------------------------------
            
            //Need to loop over Phone Calls.
            var scomplete = true;
            if (uar.SecondCallsPlaced==true) {
                var snumbers = this.main.data.filter(this.main.data.getChildren(uar),"cid EQ phonecall");
                for (var i=0,l=snumbers.length;i<l;i++) {
                    var sn = snumbers[i];
                    if (sn.SecondLocNumbersCalledForm_completed) {
                    } else {
                        scomplete = false;
                    }
                }
            }
            
            //Recheck Phone Calls list.
            //Might need to move this into a rule that only works when Blood data is being added.
            var dvw = sp.component.down("xdataview[name='uarsecondnumberscalledlist']");
            if (dvw) dvw.refresh();
            
            //secondlocationvisited
            if (uar.secondlocationvisited=="Yes") {
                this.checkForm(Ext.apply({dataprovider:uar,title: "Second Location",container: "SecondLocationTabPanel",completed: [scomplete,uar.SecondLocationLocationForm_completed,uar.SecondLocationTimeForm_completed,uar.SecondLocationCommentsForm_completed]},obj));
            } else if (uar.secondlocationvisited=="No") {
                this.checkForm(Ext.apply({dataprovider:uar,title:'Second Location',container:'SecondLocationTabPanel'},obj));
            } else {
                this.checkForm(Ext.apply({dataprovider:uar,title:'Second Location',container:'SecondLocationTabPanel',completed: [uar.SecondLocationLocationForm]},obj));
            }
            
            //Location
            if (uar.secondlocationvisited=="Yes") {
                this.checkSection(Ext.apply({title: "Location",container: "SecondLocationLocationForm",managers:["SecondLocationLocationForm"],required: true},obj));
            } else {
                this.checkSection(Ext.apply({title: "Location",container: "SecondLocationLocationForm",required: true},obj));
            }
            
            //Time
            if (uar.secondlocationvisited=="Yes") {
                this.checkSection(Ext.apply({title: "Time",container: "SecondLocationTimeForm",managers:["SecondLocationTimeForm"],required: true},obj));
            } else {
                this.checkSection(Ext.apply({title: "Time",container: "SecondLocationTimeForm",required: true},obj));
            }

            
            //Person Contacted
            this.checkSection(Ext.apply({title: "Person Contacted",container: "SecondLocationContactedForm",required: true},obj));
            
            //Restricted Access
            this.checkSection(Ext.apply({title: "Restricted Access",container: "SecondLocationRestrictedAccessForm",required: true},obj));
            
            //Numbers Called
            if (uar.secondlocationvisited=="Yes") {
                this.setComplete(Ext.apply({title: "Numbers Called",container: "SecondLocNumbersCalledForm",completed: scomplete},obj));
            } else {
                this.checkSection(Ext.apply({title: "Numbers Called",container: "SecondLocNumbersCalledForm",required: true},obj));
            }

            
            //Comments
            if (uar.secondlocationvisited=="Yes") {
                this.checkSection(Ext.apply({title: "Comments",container: "SecondLocationCommentsForm",managers:["SecondLocationCommentsForm"],required: true},obj));
            } else {
                this.checkSection(Ext.apply({title: "Comments",container: "SecondLocationCommentsForm",required: true},obj));
            }
            
            
            //Comments
            //--------------------------------
            this.checkForm(Ext.apply({dataprovider:uar,title:'Comments',container:'uarcommentsform'},obj));
            
            //DCO Confirmation
            //--------------------------------
            this.checkForm(Ext.apply({dataprovider:uar,title:'DCO<br>Confirmation',container:'dcoconfirmationform',completed:[uar.dcoconfirmationform_completed]},obj));
            
            //Check completed status on all forms
            this.checkCollection(Ext.apply({button:'submituarbtn',completed:[uar.uarathleteinfoform_completed,uar.attemptinfoform_completed,uar.FirstLocationTabPanel_completed,uar.SecondLocationTabPanel_completed,uar.dcoconfirmationform_completed]},obj));
            

            //Done
            this.main.actions.perform({
                cmd: 'removebusy',
                component: sp.component
            });
            
        },1000,this);

    },
    
     checkaarHandler: function (sp) {
    
        //This function is called after rule's component (AAR tabpanel) is added to the screen.
        //It is also called every time the completed context changes from any of the managers in the AAR section.
        
        this.main.actions.perform({
            cmd: 'setbusy',
            component: sp.component,
            message: 'Checking progress...'
        });
        
        //Important: Defer to allow all components to draw on screen, then must set up all managers in
        //manageProviders before this function will work correctly.
        
        /* Common pitfalls so far:
            incorrect provider
            provider must be character exact
            must separate providers and managers into trees so they do not overlap.
            provider name is case sensitive and must be exact
            forgetting to put required:true on a component
            
        */

        Ext.defer(function () {
        
            var obj = {
                component: sp.component,
                subtopic: (sp.rule) ? sp.rule.value : ''
            }
            
            //container = name of the component you want to mark with complete/incomplete style.
            //manager = name of a manager component that is managing a section of fields and marking them complete/incomplete
            //title = base title to alter with complete/incomplete style.
            //required = if true it will ignore started setting and track the completeness of that section no matter what.
            
            var aar = this.main.data.getProvider("{activeaar}");
            
            //Location of Completed Test
            //--------------------------------
            this.checkForm(Ext.apply({dataprovider:aar,title:'Location of<br/>Completed Test',container:'locationofcompletedtestform',completed:[aar.locationofcompletedtestform_completed]},obj));
            

            //Other Locations
            //--------------------------------
            this.checkForm(Ext.apply({dataprovider:aar,title:'Other<br/>Locations',container:'otherlocationsform'},obj));
            
            
            //Notification
            //--------------------------------
            this.checkForm(Ext.apply({dataprovider:aar,title:'Notification',container:'aarnotificationform',completed:[aar.aarnotificationform_completed]},obj));
            
            //Processing
            //--------------------------------
            this.checkForm(Ext.apply({dataprovider:aar,title:'Processing',container:'aarprocessingform',completed:[aar.aarprocessingform_completed]},obj));
            
            //Post Processing
            //--------------------------------
            this.checkForm(Ext.apply({dataprovider:aar,title:'Post<br/>Processing',container:'aarpostprocessingform'},obj));
            
            //Comments
            //--------------------------------
            this.checkForm(Ext.apply({dataprovider:aar,title:'Comments',container:'aarcommentsform'},obj));
            
            //Signature
            //--------------------------------
            this.checkForm(Ext.apply({dataprovider:aar,title:'Signature',container:'aarsignatureform',completed:[aar.aarsignatureform_completed]},obj));
            
            //Check completed status on all forms
            this.checkCollection(Ext.apply({button:'submitaarbutton',completed:[aar.locationofcompletedtestform_completed,aar.aarnotificationform_completed,aar.aarprocessingform_completed,aar.aarsignatureform_completed]},obj));

            //Done
            this.main.actions.perform({
                cmd: 'removebusy',
                component: sp.component
            });
            
        },1000,this);

    },
    
    checkSection: function (sp) {
    
        //Check a section for completeness and set style.
    
        try {
        
            var managers = sp.managers;
            var started = false;
            var completed = true;
            
            if (Ext.isArray(managers)) {
            
                //Combine all the different managers to create a complete/started condition for all managers.
                for (var i=0,l=managers.length;i<l;i++) {
                    var n = managers[i];
                    var manager = this.main.data.getObjectByName(n);
                    var provider = this.main.data.getProvider(manager.manage);
                    if (provider) {
                        started = started||(provider[n+"_started"]);
                        completed = completed&&(provider[n+"_completed"]);
                    } else {
                        console.log("couldn't find provider " + manager.manage);
                    }
                }

                var container = this.main.data.getObjectByName(sp.container);
                if (started||sp.required) {
                    var selector = container.cid + "[name='" + sp.container + "']";
                    var o = sp.component.down(selector);
                    if (completed) {
                        o.setTitle('<span class="fa fa-check-circle" style="color:green;"></span> ' + sp.title);
                    } else {
                        o.setTitle('<span class="fa fa-ban" style="color:red;"></span> ' + sp.title);
                    }
                }
               
            } else {
           
                //Enable this section if you want to have all tabs with checkmarks even if there are no required fields.
                //Leave this section disabled if you want to only put checkmarks on tabs with required information.
           
           
                try {
                    //Manual override to give green checks to sections that don't have any required fields.
                    var container = this.main.data.getObjectByName(sp.container);
                    var selector = container.cid + "[name='" + sp.container + "']";
                    var o = sp.component.down(selector);
                    o.setTitle('<span class="fa fa-circle" style="color:grey;"></span> ' + sp.title);
                } catch (e) {
                    console.log("Couldn't locate " + selector);
                }
           
            }
        
        } catch (e) {
            //debugger;
        }
        
    },
    
    setComplete: function (sp) {
        //Manual switch for special cases.
        var container = this.main.data.getObjectByName(sp.container);
        var selector = container.cid + "[name='" + sp.container + "']";
        var o = sp.component.down(selector);
        if (sp.completed) {
            o.setTitle('<span class="fa fa-check-circle" style="color:green;"></span> ' + sp.title);
        } else {
            o.setTitle('<span class="fa fa-ban" style="color:red;"></span> ' + sp.title);
        }
    
    },
    
    checkForm: function (sp) {
        //Manual check for main forms.
        if (!sp.completed) sp.completed = [true];
        if (!sp.disabled) sp.disabled = false;
    
        var v = sp.completed.every(function(i){
            return i;
        });
    
        var frm = sp.component.down("xtabpanel[name='" + sp.container + "']")||sp.component.down("xform[name='" + sp.container + "']");
    
        var ap = {
            cmd: "put",
            component: this,
            objects: [sp.dataprovider]
        };
        
        ap[sp.container+"_completed"] = v;
        this.main.actions.perform(ap);

        if (frm) {
            if (sp.disabled) {
                frm.setTitle('<span class="fa fa-circle" style="color:grey;"></span> ' + sp.title);
            } else {
                if (v) {
                    frm.setTitle('<span class="fa fa-check-circle" style="color:green;"></span> ' + sp.title);
                } else {
                    frm.setTitle('<span class="fa fa-ban" style="color:red;"></span> ' + sp.title);
                }
            }
        }

    },
    
    checkCollection: function (sp) {
        if (!sp.completed) sp.completed = [true];
        
        var v = sp.completed.every(function(i){
            return i;
        });
        
        var btn = sp.component.down("xbutton[name='" + sp.button + "']");
           
        if (btn) {
            if (v) {
                btn.enable(true);
            } else {
                btn.disable(true);
            }
        }
    }
           
});

Ext.ClassManager.addNameAliasMappings({
    'Paperless.view.main.Actions': ['custom']
});